import React, { useEffect, useState, Suspense, useRef } from 'react';
import { GetAllUsers, DeleteUser } from '../../APIs';
import './style.css';

const UserAddEdit = React.lazy(() => import('./addEdit'));

export default () => {
    const [users, setUsers] = useState([])
    const [showAddEdit, toggleShowAddEdit] = useState(false)
    const [reload, toggleReload] = useState(false)
    const userID = useRef(null)

    useEffect(() => {
        GetAllUsers().then(response => {
            setUsers(response.data)
        })
    }, [reload])

    const deleteUser = (userID) => {
        DeleteUser(userID).then(() => {
            toggleReload(r => !r)
        })
    }

    return (
        <>
            <div className='users'>
                <button className='button createBtn'
                    onClick={() => { userID.current = null; toggleShowAddEdit(s => !s) }}>
                    Create User
                </button>

                <table className='table'>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map(u =>
                            <tr key={u.id}>
                                <td>{u.name}</td>
                                <td>{u.email}</td>
                                <td>
                                    <button className='button editBtn'
                                        onClick={() => { userID.current = u.id; toggleShowAddEdit(s => !s) }}>
                                        Edit
                                    </button>

                                    <button className='button deleteBtn'
                                        onClick={() => deleteUser(u.id)}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>

            <Suspense fallback={<div></div>}>
                <UserAddEdit show={showAddEdit} hide={() => toggleShowAddEdit(s => !s)}
                    userID={userID.current} reload={() => toggleReload(r => !r)} />
            </Suspense>
        </>
    );
}