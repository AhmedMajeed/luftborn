import React, { useEffect, useState } from 'react';
import Popup from '../../../components/popup';
import Field from '../../../components/field';
import { CreateUser, UpdateUser, GetUser } from '../../../APIs';
import { ValidateEmail } from '../../../utils';
import './style.css';

export default ({ show, hide, userID, reload }) => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [isLoading, setIsloading] = useState(false)
    const [errorMsg, setErrorMsg] = useState('')

    useEffect(() => {
        if (userID) {
            GetUser(userID).then(response=>{
                let user = response.data
                setName(user.name)
                setEmail(user.email)
            })
        }
        else {
            setName('')
            setEmail('')
        }
    }, [userID])

    const saveUser = () => {
        setErrorMsg('')
        if (name.trim() === '' || email.trim() === '') {
            setErrorMsg('Please fill all fields')
            return
        }

        if (!ValidateEmail(email)) {
            setErrorMsg('Invalid Email')
            return
        }

        setIsloading(true)
        if (userID)
            UpdateUser(userID, name, email).then(()=>{
                finishOperation()
            })
        else
            CreateUser(name, email).then(()=>{
                finishOperation()
            })
    }

    const finishOperation = () => {
        setIsloading(false)
        hide()
        reload()
    }

    return (
        <Popup show={show} className='userAddEdit' modalID='addEditUserModal' title={`${!userID ? 'Add' : 'Edit'} User`}>
            <div className='row'>
                <div className='col-sm-6'>
                    <Field value={name} setValue={setName} placeholder='write name' />
                </div>
                <div className='col-sm-6'>
                    <Field value={email} setValue={setEmail} placeholder='write email' />
                </div>
            </div>
            {errorMsg && <div className='errorMsg'>{errorMsg}</div>}

            <button className='button' onClick={saveUser} disabled={isLoading}>
                <span className={isLoading ? "spinner-border spinner-border-sm" : ""} role="status" aria-hidden="true"></span>
                {!userID ? 'Add' : 'Edit'}
            </button>
        </Popup>
    );
}