import React from 'react';
import { Switch } from 'react-router-dom';
import Header from '../components/header';
import Users from '../containers/users';
import { Route } from 'react-router-dom';
import './style.css';

export default () => {
    return (
        <Switch>
            <>
                <div className='wrapper'>
                    <div className='container'>
                        <Header />

                        <Switch>
                            <Route path="/" component={Users} />
                            <Route path='*' render={() => <div>Not Found</div>} />
                        </Switch>
                    </div>
                </div>
            </>
        </Switch>
    );
}