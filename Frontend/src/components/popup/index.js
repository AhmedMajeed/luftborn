import React, { useEffect, useRef } from 'react';
import './style.css';

export default ({ modalID, show, className, title, children }) => {
    const btnRef = useRef()
    const initialized = useRef(false)

    useEffect(() => {
        if (initialized.current)
            btnRef.current.click()
        initialized.current = true
    }, [show])

    return (
        <>
            <button className='modalBtn' data-toggle="modal" data-target={`#${modalID}`}
                ref={btnRef}></button>

            <div className={`modal fade ${className}`} id={modalID} aria-hidden="true">
                <div className='modal-dialog modal-md'>
                    <div className="modal-content">
                        <div className='modal-body'>
                            <div className='headline'>
                                <span>{title}</span>
                                <span data-dismiss="modal" className='closeBtn'><i className='fas fa-times'></i></span>
                            </div>

                            {children}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}