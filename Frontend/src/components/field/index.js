import React from 'react';
import './style.css';

export default ({ type, value, setValue, placeholder }) => {
    return (
        <input className="field" type={type || 'text'} placeholder={placeholder}
            value={value} onChange={(e) => setValue(e.target.value)} />
    );
}