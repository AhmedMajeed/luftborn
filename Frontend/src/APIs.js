import axios from 'axios';

const BaseURL = 'https://localhost:7209/api/'

export const GetAllUsers = () => {
    return axios.get(BaseURL + 'user')
}

export const GetUser = (userID) => {
    return axios.get(BaseURL + 'user/' + userID)
}

export const CreateUser = (Name, Email) => {
    return axios.post(BaseURL + 'user/', {
        Name, Email
    }).then(response => response.data)
}

export const UpdateUser = (userID, Name, Email) => {
    return axios.put(BaseURL + 'user/' + userID, {
        Name, Email
    })
}

export const DeleteUser = (userID) => {
    return axios.delete(BaseURL + 'user/' + userID)
}
