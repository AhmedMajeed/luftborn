﻿using ApplicationCore.Entities;
using System.Linq.Expressions;

namespace ApplicationCore.Interfaces
{
    public interface IUserService
    {
        IQueryable<User> Find(Expression<Func<User, bool>> expression);

        User GetByID(int id);

        User Insert(User entity);

        void Update(User updatedUser);

        void Delete(int id);
    }
}
