﻿using System.ComponentModel.DataAnnotations;

namespace DTOs.User
{
    public class CUUserDTO
    {
        [Required]
        public string Name { get; set; }

        [RegularExpression(@"^(([^<>()[\]\\.,;:\s@""]+(\.[^<>()[\]\\.,;:\s@""]+)*)|("".+""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$",
            ErrorMessage = "InvalidEmail")]
        public string Email { get; set; }
    }
}
