﻿
namespace DTOs.User
{
    public class DataUserDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
