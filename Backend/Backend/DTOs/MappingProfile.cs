﻿using AutoMapper;
using DTOs.User;
using E = ApplicationCore.Entities;

namespace DTOs
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region user
            CreateMap<E.User, DataUserDTO>();
            CreateMap<CUUserDTO, E.User>();
            #endregion
        }
    }
}
