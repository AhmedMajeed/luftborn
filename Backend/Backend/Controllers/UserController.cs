using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using AutoMapper;
using DTOs.User;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        readonly IUserService _userService;
        readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult Create(CUUserDTO userDTO)
        {
            User newUser = _mapper.Map<User>(userDTO);
            var createdUser = _userService.Insert(newUser);

            return Ok(_mapper.Map<DataUserDTO>(createdUser));
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.Find(u => true);

            return Ok(users.Select(_mapper.Map<DataUserDTO>));
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var user = _userService.GetByID(id);

            DataUserDTO userDataDto = _mapper.Map<DataUserDTO>(user);
            return Ok(userDataDto);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateById(int id, CUUserDTO userDTO)
        {
            User updatedUser = _mapper.Map<User>(userDTO);
            updatedUser.Id = id;

            _userService.Update(updatedUser);
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteById(int id)
        {
            _userService.Delete(id);
            return Ok();
        }
    }
}
