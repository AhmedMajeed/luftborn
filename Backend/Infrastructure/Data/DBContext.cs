﻿using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Infrastructure.Data
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions options) : base(options)
        { }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Users
            modelBuilder.Entity<User>(u =>
            {
                u.Property(u => u.Id).UseIdentityColumn().HasColumnOrder(0);
                u.Property(m => m.Name).HasMaxLength(200).HasColumnOrder(1);
            });

            modelBuilder.Entity<User>().HasData(
               new User { Id = 1, Name = "Majied", Email="a@gmail.com" }
            );
            #endregion
        }
    }
}