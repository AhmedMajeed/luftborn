﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Infrastructure.Data;
using System.Linq.Expressions;

namespace Infrastructure.Services
{
    public class UserService : IUserService
    {
        protected readonly DBContext _context;

        public UserService(DBContext context)
        {
            _context = context;
        }

        public IQueryable<User> Find(Expression<Func<User, bool>> expression)
        {
            return _context.Users.Where(expression).OrderByDescending(u => u.Id);
        }

        public User GetByID(int id)
        {
            var entity = _context.Users.Where(e => e.Id == id).SingleOrDefault();
            return entity == null ? throw new KeyNotFoundException() : entity;
        }

        public User Insert(User entity)
        {
            var newUser = _context.Users.Add(entity).Entity;
            _context.SaveChanges();
            return newUser;
        }

        public void Update(User updatedUser)
        {
            _context.Users.Update(updatedUser);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            User? user = GetByID(id);
            _context.Users.Remove(user);
            _context.SaveChanges();
        }
    }
}
